const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("votaminDB", "root", "", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestamps: true,
    },
    timezone: "+00:00",
});

module.exports = sequelize;