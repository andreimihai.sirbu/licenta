const Sequelize = require("sequelize").Sequelize;

const UserDB = require("../models/").User;
const PollDB = require("../models/").Poll;
const VoteDB = require("../models").Vote;
const OptionsDB = require("../models").Options;
const crypto = require("crypto");
const bcrypt = require("bcrypt");

const multer = require("multer");

const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "../csv");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "--" + file.originalname);
    },
});

const upload = multer({ storage: fileStorageEngine });

function createJSON() {}

const controller = {
    createPoll: async(req, res) => {
        var randomToken = crypto.randomBytes(3).toString("hex");
        randomToken = randomToken.toUpperCase();

        const poll = {
            tkn: randomToken,
            question: req.body.data.question,
            desc: req.body.data.description,
            majority: req.body.data.majority,
            access_lvl: req.body.data.access_lvl,
            required_rank: req.body.data.required_rank,
            open_date: req.body.data.open_date,
            close_date: req.body.data.close_date,
            poll_status: "closed",
            result: "TBD",
            quorum: -1,
        };

        let options = req.body.data.options;

        var optionObjs = [];

        for (i = 0; i < options.length; i++) {
            tmp = {
                poll_tkn: poll.tkn,
                text: options[i],
                option_no: i + 1,
            };

            optionObjs.push(tmp);
        }

        try {
            const newPoll = await PollDB.create(poll);

            try {
                for (i = 0; i < optionObjs.length; i++) {
                    await OptionsDB.create(optionObjs[i]);
                }
            } catch (err) {
                res.status(500).send({ message: "Failed to insert into Options" });

                //we delete the row from POLLS, since it's useless without the respective data in OPTIONS
                try {
                    const pollObj = await PollDB.findOne({
                        where: {
                            tkn: poll.tkn,
                        },
                    });
                    await pollObj.destroy();
                } catch (err) {
                    res.status(500).send({
                        message: "Failed to delete poll as result of failure to insert into Options",
                    });
                }
            }

            res.status(200).send({ message: "Poll created!", poll_tkn: newPoll.tkn });
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    },

    deletePoll: async(req, res) => {
        const poll = await PollDB.findOne({
            where: {
                tkn: req.body.tkn,
            },
        });

        if (poll) {
            try {
                const pollOptions = await OptionsDB.findAll({
                    where: {
                        poll_tkn: req.body.tkn,
                    },
                });

                for (let i = 0; i < pollOptions.length; i++) {
                    await pollOptions[i].destroy();
                }

                const pollVotes = await VoteDB.findAll({
                    where: {
                        poll_tkn: req.body.tkn,
                    },
                });

                for (let i = 0; i < pollVotes.length; i++) {
                    await pollVotes[i].destroy();
                }

                await poll.destroy();

                res.status(200).send({ message: "Poll deleted" });
            } catch {
                res.status(500).send({ message: "Server error" });
            }
        } else {
            res.status(400).send({ message: "Poll does not exist" });
        }
    },

    getAllPolls: async(req, res) => {
        try {
            const polls = await PollDB.findAll();
            // console.log(polls);

            res.status(200).send({ message: "Polls retrieved", polls: polls });
        } catch (error) {
            res.status(500).send({ message: "Server error when retrieving polls" });
        }
    },

    getAllUsers: async(req, res) => {
        try {
            const users = await UserDB.findAll();
            res.status(200).send({ message: "Users retrieved", users: users });
        } catch (error) {
            res.status(500).send({ message: "Server error when retrieving users" });
        }
    },

    uploadCSV: async(req, res) => {
        try {
            console.log(req.params.fileData);
            res.status(200).send({ message: "CSV Upload successfull" });
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when uploading CSV file to server storage",
            });
        }
    },
    generateUsersFromCSV: async(req, res) => {
        try {
            const filename = req.body.filename;
            console.log(filename);

            var fs = require("fs");
            var csv = require("csv-parser");

            var results = [];

            var parser = csv({ columns: true }, function(err, records) {
                // console.log(records);

                for (let i = 0; i < records.length; i++) {
                    var user = {
                        username: records[i].username,
                        password: records[i].username,
                        isAdmin: records[i].isAdmin,
                        dep_code: records[i].dep_code,
                        hire_date: records[i].hire_date,
                        position: records[i].position,
                        jobRank: records[i].jobRank,
                        hasEverLoggedIn: false,
                    };
                }
            });

            fs.createReadStream("./csv/" + filename)
                .pipe(csv({ separator: ";" }))
                .on("data", async(data) => {
                    var user = {
                        username: data.username,
                        password: data.username,
                        isAdmin: data.isAdmin,
                        dep_code: data.dep_code,
                        hire_date: data.hire_date,
                        position: data.position,
                        jobRank: data.jobRank,
                        hasEverLoggedIn: false,
                    };
                    results.push(user);
                    try {
                        // console.log(user);
                        user.password = await bcrypt.hash(user.password, 10);
                        await UserDB.create(user);
                    } catch (error) {
                        console.log(error);
                        return res
                            .status(500)
                            .send({ message: "Server error when creating a user" });
                    }
                })
                .on("end", () => {
                    // console.log(results);
                });

            res.status(200).send({ message: "Users created successfully" });

            // console.log("usersData: ");
            // console.log(usersData);

            // console.log("results: ==================");
            // console.log(results);

            // for (let i = 0; i < results.length; i++) {
            //     var user = {
            //         username: usersData[i].username,
            //         password: usersData[i].username,
            //         isAdmin: usersData[i].isAdmin,
            //         dep_code: usersData[i].dep_code,
            //         hire_date: usersData[i].hire_date,
            //         position: usersData[i].position,
            //         jobRank: usersData[i].jobRank,
            //         hasEverLoggedIn: false,
            //     };

            //     user.password = await bcrypt.hash(req.body.password, 10);

            //     try {
            //         // console.log(user);
            //         // await UserDB.create(user);
            //         // console.log("User registered successfully");
            //     } catch (error) {
            //         console.log(error);
            //         return res
            //             .status(500)
            //             .send({ message: "Server error when creating a user" });
            //     }
            // }

            // res
            //     .status(200)
            //     .send({ message: "Users generated successfully from CSV" });
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when generating users from CSV file",
            });
        }
    },
};

module.exports = controller;