const users = require("./users");
const polls = require("./polls");
const admin = require("./admin");
const middleware = require("./middleware");
const other = require("./other");

const controllers = {
    users,
    polls,
    middleware,
    other,
    admin,
};

module.exports = controllers;