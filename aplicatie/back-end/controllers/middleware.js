const UserDB = require("../models").User;
const PollDB = require("../models").Poll;
const jwt = require("jsonwebtoken");

const controller = {
    // check if user is logged in (MIDDLEWARE)
    isAuthenticated: async(req, res, next) => {
        if (req.body.token == "")
            res.status(403).send({ message: "user is not authenticated" });
        else {
            console.log(req.form);
            const user = await jwt.verify(req.body.token, process.env.JWT_SECRET);
            const userId = user.id;

            try {
                const userInDB = await UserDB.findByPk(userId);
                if (!userInDB) {
                    res.status(403).send();
                } else {
                    next();
                }
            } catch (error) {
                console.log(error);
            }
        }
    },

    // check if the user is admin (MIDDLEWARE)
    isAdmin: async(req, res, next) => {
        const user = await jwt.verify(req.body.token, process.env.JWT_SECRET);
        const userId = user.id;

        try {
            const userInDB = await UserDB.findByPk(userId);
            if (userInDB.isAdmin == false) {
                res.status(403).send();
            } else {
                next();
            }
        } catch (error) {
            console.log(error);
        }
    },

    hasAccessToPoll: async(req, res, next) => {
        console.log("===hasAccess:");
        console.log(req.body);
        const user = await jwt.verify(req.body.token, process.env.JWT_SECRET);
        const userId = user.id;

        try {
            const userInDB = await UserDB.findByPk(userId);
            const poll = await PollDB.findByPk(req.body.polltkn);

            var hasAccess = true;

            if (poll) {
                if (userInDB.isAdmin) {
                    req.body.hasAccess = true;
                    next();
                } else {
                    console.log("poll exista");
                    console.log(userInDB.jobRank + " " + poll.required_rank);
                    if (userInDB.jobRank < poll.required_rank) {
                        hasAccess = false;
                    }

                    if (poll.access_lvl != "public" && poll.access_lvl != "board") {
                        if (poll.access_lvl != userInDB.dep_code) hasAccess = false;
                    }

                    if (hasAccess == false) {
                        console.log("has access e false");
                        req.body.hasAccess = false;
                        next();
                        // console.log("ar trebui sa dea 403");
                        // res
                        //     .status(403)
                        //     .send({ message: "User does not have access to poll" });
                    } else if (hasAccess == true) {
                        console.log(")))))))))))))))))))))))))))))))))))))");
                        console.log("has access e true");
                        req.body.hasAccess = true;
                        next();
                    }
                }
            } else {
                console.log("poll nu exista");
                res.status(404).send({ error: "Poll does not exist!" });
            }
        } catch (error) {
            console.log(error);
        }
    },
};

module.exports = controller;