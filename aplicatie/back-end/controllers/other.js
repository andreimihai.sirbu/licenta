const connection = require("../models").connection;
const UserDB = require("../models").User;
const DepartmentDB = require("../models").Department;

const controller = {
    reset: (req, res) => {
        connection
            .sync({ force: true })
            .then(async() => {
               await DepartmentDB.create({ dep_code: "IT", dep_name: "IT" });
                await UserDB.create({
                    username: "admin",
                    password: "root123",
                    isAdmin: true,
                    dep_code: "IT",
                    hire_date: new Date(),
                    position: "Themis Admin",
                    jobRank: 3,
                    hasEverLoggedIn: false,
                });

                res.status(201).send({
                    message: "Database reset",
                });
            })
            .catch(() => {
                res.status(500).send({
                    message: "Reset DB error",
                });
            });
    },
    hello: (req, res) => {
        res.status(200).send({ message: "Hello world!" });
    },
};

module.exports = controller;