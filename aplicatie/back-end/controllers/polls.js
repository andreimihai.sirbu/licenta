const Sequelize = require("sequelize").Sequelize;

const UserDB = require("../models/").User;
const PollDB = require("../models/").Poll;
const VoteDB = require("../models").Vote;
const OptionsDB = require("../models").Options;
const crypto = require("crypto");

function createJSON() {}

const controller = {
    // createPoll: async(req, res) => {
    //     var randomToken = crypto.randomBytes(3).toString("hex");
    //     randomToken = randomToken.toUpperCase();

    //     console.log(req.body);

    //     const poll = {
    //         tkn: randomToken,
    //         question: req.body.data.question,
    //         desc: req.body.data.description,
    //         majority: req.body.data.majority,
    //         access_lvl: req.body.data.access_lvl,
    //         required_rank: req.body.data.required_rank,
    //         open_date: req.body.data.open_date,
    //         close_date: req.body.data.close_date,
    //         poll_status: "closed",
    //         result: "",
    //     };

    //     let options = req.body.data.options;

    //     var optionObjs = [];

    //     for (i = 0; i < options.length; i++) {
    //         tmp = {
    //             poll_tkn: newPoll.tkn,
    //             text: options[i],
    //             option_no: i + 1,
    //         };

    //         optionObjs.push(tmp);
    //     }

    //     try {
    //         console.log("====================================");
    //         console.log(poll);
    //         newPoll = await PollDB.create(poll);
    //         res.status(200).send({ message: newPoll });

    //         // newPoll.poll_status = "closed";
    //         // newPoll.required_rank = req.body.data.required_rank;
    //         // newPoll.result = "TBD";
    //         // await newPoll.save();

    //         // try {
    //         //     for (i = 0; i < optionObjs.length; i++) {
    //         //         await OptionsDB.create(optionObjs[i]);
    //         //     }
    //         // } catch (err) {
    //         //     res.status(500).send({ message: "Failed to insert into Options" });

    //         //     //we delete the row from POLLS, since it's useless without the respective data in OPTIONS
    //         //     try {
    //         //         const pollObj = await PollDB.findOne({
    //         //             where: {
    //         //                 tkn: newPoll.tkn,
    //         //             },
    //         //         });
    //         //         await pollObj.destroy();
    //         //     } catch (err) {
    //         //         res.status(500).send({
    //         //             message: "Failed to delete poll as result of failure to insert into Options",
    //         //         });
    //         //     }
    //         // }

    //         // res.status(200).send({ message: "Poll created!" });
    //     } catch (err) {
    //         console.log("=======================");
    //         res.status(500).send(err);
    //     }
    // },

    getPoll: async(req, res) => {
        console.log("=====getPoll");
        console.log(req.body);
        try {
            const poll = await PollDB.findByPk(req.body.tkn);
            if (poll) {
                const options = await OptionsDB.findAll({
                    where: {
                        poll_tkn: poll.tkn,
                    },
                });

                res.status(200).send({
                    message: "Poll retrieved successfully",
                    poll: poll,
                    options: options,
                });
            } else {
                res.status(404).send({ message: "Poll does not exist" });
            }
        } catch (error) {
            res.status(500).send({ message: "Server error when retrieving poll" });
        }
    },

    deletePoll: async(req, res) => {
        const poll = await PollDB.findByPk(req.body.tkn);

        if (poll) {
            try {
                const pollOptions = await OptionsDB.findAll({
                    where: {
                        poll_tkn: req.body.tkn,
                    },
                });

                for (let i = 0; i < pollOptions.length; i++) {
                    await pollOptions[i].destroy();
                }

                const pollVotes = await VoteDB.findAll({
                    where: {
                        poll_tkn: req.body.tkn,
                    },
                });

                for (let i = 0; i < pollVotes.length; i++) {
                    await pollVotes[i].destroy();
                }

                await poll.destroy();

                res.status(200).send({ message: "Poll deleted" });
            } catch {
                res.status(500).send({ message: "Server error" });
            }
        } else {
            res.status(400).send({ message: "Poll does not exist" });
        }
    },

    checkPollExists: async(req, res) => {
        const poll = await PollDB.findOne({ where: { poll_tkn: req.body.token } });
        if (poll) {
            res.status(200).send({ message: "true" });
        } else {
            res.status(400).send({ message: "false" });
        }
    },

    checkIfPollExists: async(req, res, next) => {
        try {
            console.log("====checkIfPollExists:");
            console.log(req.body);
            if (req.body.hasAccess == false) {
                res.status(403).send({ hasAccess: false });
            } else if (req.body.hasAccess == true) {
                const poll = await PollDB.findByPk(req.body.polltkn);
                console.log("====poll");
                console.log(poll);
                if (poll) {
                    if (poll.poll_status == "closed" || poll.poll_status == "finished") {
                        res.send({ pollExists: true, pollInaccessible: true });
                    } else if (poll.poll_status == "in progress") {
                        res.send({ pollExists: true, pollInaccessible: false });
                    }
                    // req.pollExists = true;
                    // next();
                } else {
                    res.status(404).send({ pollExists: false });
                }
            }
        } catch (error) {
            res.status(500).send({ message: "Internal server error" });
        }
    },

    // getPoll: async(req, res) => {
    //     try {
    //         const poll = await PollDB.findByPk(req.body.polltkn);
    //         if (poll) {
    //             const options = await OptionsDB.findAll({
    //                 where: {
    //                     poll_tkn: req.body.polltkn,
    //                 },
    //             });
    //             res.status(200).send({
    //                 question: poll.question,
    //                 description: poll.desc,
    //                 options: options,
    //             });
    //         }
    //     } catch (error) {
    //         res.status(500).send({ message: "Internal server error" });
    //     }
    // },

    // updatePollStatus: async(req, res) => {
    //     console.log("a intrat pe updatePollStatus");
    //     try {
    //         const polls = await PollDB.findAll();
    //         polls.forEach(async(poll) => {
    //             if (poll.open_date > req.body.current_date) {
    //                 poll = await poll.update({ poll_status: "closed", result: "TBD" });
    //             }
    //             if (
    //                 poll.open_date < req.body.current_date &&
    //                 poll.close_date > req.body.current_date
    //             ) {
    //                 poll = await poll.update({
    //                     poll_status: "in progress",
    //                     result: "TBD",
    //                 });
    //             }
    //             if (poll.close_date < req.body.current_date) {
    //                 poll = await poll.update({ poll_status: "finished" });
    //                 if (poll.result == "TBD") {
    //                     result = await determineResult(poll.tkn);
    //                     if (result == null)
    //                         res.status(500).send({
    //                             message: "Server Error when determining result of " + poll.tkn,
    //                         });
    //                     else {
    //                         poll.result = result.option;
    //                         poll.save();
    //                     }
    //                 }
    //             }
    //         });

    //         res.status(200).send({ message: "Polls' Status updated" });
    //     } catch (error) {
    //         res
    //             .status(500)
    //             .send({ message: "Server Error when updating Polls' Status" });
    //     }
    // },

    async getNoOfPossibleVotes(token) {
        try {
            console.log("a intrat pe getNoPossibleVotes");

            const { Op } = require("sequelize");
            const poll = await PollDB.findByPk(token);

            var users = [];

            if (poll.access_lvl == "public") {
                users = await UserDB.findAll({
                    where: {
                        job_rank: {
                            [Op.gte]: poll.required_rank,
                        },
                    },
                });
            }

            if (poll.access_lvl == "board") {
                users = await UserDB.findAll({
                    where: {
                        dep_code: "board",
                    },
                });
            }

            if (poll.access_lvl != "public" && poll.access_lvl != "board") {
                users = await UserDB.findAll({
                    where: {
                        job_rank: {
                            [Op.gte]: poll.required_rank,
                        },
                        dep_code: poll.access_lvl,
                    },
                });
            }
            // console.log(users);
            return users.length;
        } catch (error) {
            console.log("a intrat pe catch la getNoOfPossibleVotes");
            console.log(error);
            return null;
        }
    },

    async determineResult(token) {
        try {
            console.log("a intrat pe determineResult pt" + token);

            const poll = await PollDB.findByPk(token);

            if (!poll) {
                console.log("nu a existat poll");
                return null;
            }
            // var quorum = await this.getNoOfPossibleVotes(token);
            const quorum = poll.quorum;
            if (quorum == null) {
                console.log("nu a existat cvorum");
                return null;
            } else {
                var results = [];
                var options = await OptionsDB.findAll({
                    where: { poll_tkn: token },
                });

                // options.forEach(async(option) => {
                //     console.log("sper sa nu crape");
                //     var noVotes = await VoteDB.findAll({
                //         where: {
                //             poll_tkn: token,
                //             option: option.option_no,
                //         },
                //     }).length;

                //     console.log("n-a crapat");

                //     results.push({
                //         option: option.text,
                //         option_no: option.option_no,
                //         noVotes: noVotes,
                //     });
                // });

                for (let i = 0; i < options.length; i++) {
                    var noVotes = await VoteDB.count({
                        where: {
                            poll_tkn: token,
                            option: options[i].option_no,
                        },
                    });
                    console.log(options[i].option_no + ":" + noVotes);

                    results.push({
                        option: options[i].text,
                        option_no: options[i].option_no,
                        noVotes: noVotes,
                    });
                }

                let checkSum = 0;
                for (let i = 0; i < results.length; i++) {
                    checkSum += results[i].noVotes;
                }

                console.log("==========results: ");
                console.log(results);
                console.log(results.length);

                if (checkSum != quorum) {
                    console.log("crede ca sunt mai putine voturi decat cvorum");
                    console.log("checkSum: " + checkSum + " quorum: " + quorum);
                    var indexOfAbstain = results.findIndex(
                        (result) => result.option == "Abstain" || result.option == "abstain"
                    );
                    console.log("indexOfAbstain: " + indexOfAbstain);
                    if (indexOfAbstain == -1) {
                        results.push({
                            option: "Abstain",
                            option_no: 0,
                            noVotes: quorum - checkSum,
                        });
                    }
                    if (indexOfAbstain != -1) {
                        results[indexOfAbstain].noVotes += quorum - checkSum;
                    }
                }

                var maxNoVotes = Math.max.apply(
                    Math,
                    results.map(function(result) {
                        return result.noVotes;
                    })
                );

                if (poll.majority == "simple") {
                    if (maxNoVotes > quorum / 2.0) {
                        console.log("nicio varianta nu a depasit 50%");
                        console.log("maxNoVotes: " + maxNoVotes + "quorum:" + quorum / 2.0);
                        return results.find((result) => result.noVotes == maxNoVotes);
                    } else {
                        return results.find(
                            (result) =>
                            result.option == "Abstain" || result.option == "abstain"
                        );
                    }
                }

                if (poll.majority == "absolute") {
                    if (maxNoVotes > quorum * (2.0 / 3.0))
                        return results.find((result) => result.noVotes == maxNoVotes);
                    else {
                        return results.find(
                            (result) =>
                            result.option == "Abstain" || result.option == "abstain"
                        );
                    }
                }
            }
        } catch (error) {
            console.log("###############################");
            console.log(error);
            console.log("a intrat pe catch la determineResult");
            return null;
        }
    },

    async updatePollStatus(currentDate) {
        console.log("a intrat pe updatePollStatus");
        try {
            const polls = await PollDB.findAll();
            polls.forEach(async(poll) => {
                if (poll.open_date > currentDate) {
                    poll = await poll.update({ poll_status: "closed", result: "TBD" });
                }
                if (poll.open_date < currentDate && poll.close_date > currentDate) {
                    const quorum = await controller.getNoOfPossibleVotes(poll.tkn);
                    poll = await poll.update({
                        poll_status: "in progress",
                        result: "TBD",
                        quorum: quorum,
                    });
                }
                if (poll.close_date < currentDate) {
                    poll = await poll.update({ poll_status: "finished" });
                    if (poll.result == "TBD") {
                        result = await this.determineResult(poll.tkn);
                        console.log("=========================");
                        console.log(result);
                        if (result == null) {
                            console.log("result s-a intors null in updatePollStatus");
                            return null;
                        } else {
                            poll.result = result.option;
                            poll.save();
                        }
                    }
                }
            });

            return "ok";
        } catch (error) {
            console.log("a intrat pe catch in updatePollStatus");
            return null;
        }
    },

    getDetails: async(req, res) => {
        // console.log("))))))))))))))))))))))))))))");
        // console.log("a intrat pe getDetails");
        try {
            if (req.body.hasAccess == false) {
                res
                    .status(403)
                    .send({ message: "User does not have access to this poll" });
            } else if (req.body.hasAccess == true) {
                const poll_tkn = req.body.polltkn;
                const poll = await PollDB.findByPk(poll_tkn);
                if (poll) {
                    // const quorum = await controller.getNoOfPossibleVotes(poll_tkn);
                    const quorum = poll.quorum;
                    if (quorum) {
                        let results = [];
                        var options = await OptionsDB.findAll({
                            where: { poll_tkn: poll_tkn },
                        });
                        for (let i = 0; i < options.length; i++) {
                            var noVotes = await VoteDB.count({
                                where: {
                                    poll_tkn: poll_tkn,
                                    option: options[i].option_no,
                                },
                            });
                            // console.log(options[i].option_no + ":" + noVotes);

                            results.push({
                                option: options[i].text,
                                option_no: options[i].option_no,
                                noVotes: noVotes,
                                percentage: 0,
                            });
                        }

                        let checkSum = 0;
                        for (let i = 0; i < results.length; i++) {
                            checkSum += results[i].noVotes;
                        }

                        // console.log("==========results: ");
                        // console.log(results);
                        // console.log(results.length);

                        if (checkSum != quorum) {
                            // console.log("crede ca sunt mai putine voturi decat cvorum");
                            // console.log("checkSum: " + checkSum + " quorum: " + quorum);
                            var indexOfAbstain = results.findIndex(
                                (result) =>
                                result.option == "Abstain" || result.option == "abstain"
                            );
                            console.log("indexOfAbstain: " + indexOfAbstain);
                            if (indexOfAbstain == -1) {
                                results.push({
                                    option: "Abstain",
                                    option_no: 0,
                                    noVotes: quorum - checkSum,
                                    percentage: 0,
                                });
                            }
                            if (indexOfAbstain != -1) {
                                results[indexOfAbstain].noVotes += quorum - checkSum;
                            }
                        }

                        for (let i = 0; i < results.length; i++) {
                            results[i].percentage =
                                ((results[i].noVotes * 1.0) / quorum) * 100;
                        }

                        console.log(results);

                        res.status(200).send({
                            message: "Details retrieved",
                            details: results,
                            question: poll.question,
                            desc: poll.desc,
                        });
                    } else {
                        throw "Quorum was null";
                    }
                } else {
                    console.log("crede ca poll nu exista");
                    res.status(404).send({ message: "Poll does not exist" });
                }
            }
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when retrieving poll details",
            });
        }
    },

    canBeModifiedMiddleware: async(req, res, next) => {
        try {
            const poll = await PollDB.findByPk(req.body.polltkn);
            if (poll) {
                if (poll.poll_status == "finished") {
                    req.body.canBeModified = false;
                    next();
                }
                if (poll.poll_status == "in progress") {
                    const votes = await VoteDB.findAll({
                        where: {
                            poll_tkn: poll.tkn,
                        },
                    });
                    if (votes.length == 0) {
                        req.body.canBeModified = true;
                        next();
                    } else if (votes.length > 0) {
                        req.body.canBeModified = false;
                        next();
                    }
                }
                if (poll.poll_status == "closed") {
                    req.body.canBeModified = true;
                    next();
                }
            } else {
                res.status(404).send({ message: "Poll not found" });
            }
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when checking if poll can be modified",
            });
        }
    },
    canBeModified: async(req, res) => {
        console.log("==canBeModified");
        try {
            const poll = await PollDB.findByPk(req.body.tkn);
            if (poll) {
                if (poll.poll_status == "finished") {
                    res.status(200).send({ canBeModified: false });
                }
                if (poll.poll_status == "in progress") {
                    const votes = await VoteDB.findAll({
                        where: {
                            poll_tkn: poll.tkn,
                        },
                    });
                    if (votes.length == 0) {
                        res.status(200).send({ canBeModified: true });
                    } else if (votes.length > 0) {
                        res.status(200).send({ canBeModified: false });
                    }
                }
                if (poll.poll_status == "closed") {
                    res.status(200).send({ canBeModified: true });
                }
            } else {
                res.status(404).send({ message: "Poll not found" });
            }
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when checking if poll can be modified",
            });
        }
    },

    editPoll: async(req, res) => {
        try {
            if (req.body.canBeModified == true) {
                const pollData = {
                    tkn: req.body.polltkn,
                    question: req.body.data.question,
                    desc: req.body.data.description,
                    majority: req.body.data.majority,
                    access_lvl: req.body.data.access_lvl,
                    required_rank: req.body.data.required_rank,
                    open_date: req.body.data.open_date,
                    close_date: req.body.data.close_date,
                };

                await PollDB.update(pollData, {
                    where: { tkn: pollData.tkn },
                });

                await OptionsDB.destroy({
                    where: { poll_tkn: pollData.tkn },
                });

                let options = req.body.data.options;

                var optionObjs = [];

                for (i = 0; i < options.length; i++) {
                    tmp = {
                        poll_tkn: pollData.tkn,
                        text: options[i],
                        option_no: i + 1,
                    };

                    optionObjs.push(tmp);
                }
                for (let i = 0; i < optionObjs.length; i++) {
                    await OptionsDB.create(optionObjs[i]);
                }

                res.status(200).send({ message: "Poll updated!" });
            } else if (req.body.canBeModified == false) {
                res.status(403).send({
                    message: "Cannot modify poll",
                });
            }
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Internal server error when checking if poll can be modified",
            });
        }
    },
};

module.exports = controller;