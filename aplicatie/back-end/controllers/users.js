const UserDB = require("../models/").User;
const PollDB = require("../models/").Poll;
const DepartmentDB = require("../models").Department;
const VoteDB = require("../models").Vote;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// const JWT_SECRET = process.env.JWT_SECRET;

const controller = {
    // addUser: async(req, res) => {
    //     const user = {
    //         userName: req.body.userName,
    //         password: req.body.password,
    //         email: req.body.email,
    //         telefon: req.body.telefon,
    //         age: req.body.age,
    //     };

    //     try {
    //         await UserDB.create(user);

    //         res.status(200).send({ message: "Utilizator adaugat!" });
    //     } catch (err) {
    //         res.status(500).send(err);
    //     }
    // },

    // getSpecificData: async(req, res) => {
    //     await UserDB.findOne({
    //             attributes: ["email", "telefon"],
    //             where: {
    //                 userName: req.params.userName,
    //             },
    //         })
    //         .then((user) => {
    //             res.status(200).send(user);
    //         })
    //         .catch(() => {
    //             res.status(400).send({ message: "No user found" });
    //         });
    // },

    getDepartments: async(req, res) => {
        try {
            const departments = await DepartmentDB.findAll();
            // console.log(polls);

            res
                .status(200)
                .send({ message: "Departments retrieved", departments: departments });
        } catch (error) {
            res
                .status(500)
                .send({ message: "Server error when retrieving departments" });
        }
    },

    getProfile: async(req, res) => {
        try {
            const user = await req.user;
            const userData = {
                username: user.username,
                dep_code: user.dep_code,
                hire_date: user.hire_date,
                position: user.position,
            };
            res.status(200).send(userData);
        } catch (err) {
            res.status(500).send(err);
        }
    },

    registerUser: async(req, res) => {
        const user = {
            username: req.body.username,
            password: req.body.password,
            isAdmin: req.body.isAdmin,
            dep_code: req.body.dep_code,
            hire_date: req.body.hire_date,
            position: req.body.position,
            jobRank: req.body.jobRank,
            hasEverLoggedIn: false,
        };

        user.password = await bcrypt.hash(req.body.password, 10);

        try {
            const response = await UserDB.create(user);
            console.log("User registered successfully", response);
            res.status(200).send({ message: "User registered!" });
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send({ message: "Server error when creating user" });
        }
    },

    updateUser: async(req, res) => {
        const user = {
            username: req.body.username,
            isAdmin: req.body.isAdmin,
            dep_code: req.body.dep_code,
            hire_date: req.body.hire_date,
            position: req.body.position,
            jobRank: req.body.jobRank,
        };

        try {
            const response = await UserDB.update(user, {
                where: {
                    username: user.username,
                },
            });
            console.log("User registered successfully", response);
            res.status(200).send({ message: "User registered!" });
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send({ message: "Server error when creating user" });
        }
    },

    getUser: async(req, res) => {
        console.log("=========");
        console.log(req.body);

        try {
            const user = await UserDB.findOne({
                where: {
                    username: req.body.username,
                },
            });
            res
                .status(200)
                .send({ message: "User retrieved successfully!", user: user });
        } catch (error) {
            console.log(error);
            res.status(500).send({ message: "Server error when retrieving user" });
        }
    },

    loginUser: async(req, res) => {
        // console.log(process.env);
        const loginData = {
            username: req.body.username,
            password: req.body.password,
        };

        // console.log("se buleste la findOne");
        const user = await UserDB.findOne({
            where: {
                username: loginData.username,
            },
        });

        if (user) {
            // console.log("se buleste bcrypt");
            if (await bcrypt.compare(loginData.password, user.password)) {
                const token = jwt.sign({
                        id: user.id,
                        username: user.username,
                    },
                    process.env.JWT_SECRET
                );

                // console.log(user);
                ok = true;
                if (user.hasEverLoggedIn == true) ok = false;

                return res.status(200).send({
                    message: "Login successful",
                    data: token,
                    user: user,
                    firstLogin: ok,
                });
            }

            return res
                .status(401)
                .send({ message: "Invalid username/password combination" });
        }
        return res
            .status(401)
            .send({ message: "Invalid username/password combination" });
    },

    changePassword: async(req, res) => {
        const reqData = {
            token: req.body.token,
            plainTextPass: req.body.newPassword,
        };
        try {
            const user = await jwt.verify(reqData.token, process.env.JWT_SECRET);
            const userId = user.id;
            const newPassword = await bcrypt.hash(reqData.plainTextPass, 10);

            await UserDB.update({
                password: newPassword,
                hasEverLoggedIn: true,
            }, {
                where: { id: userId },
            });

            res.status(202).send({ message: "Password updated" });
        } catch (err) {
            res.status(500).send({ message: err });
        }
    },

    // vote: async(req, res) => {
    //     try {
    //         const token = req.body.polltkn;
    //         const option = req.body.option;
    //         const user = jwt.verify(req.body.token, process.env.JWT_SECRET);
    //         const username = await bcrypt.hash(
    //             user.username,
    //             Math.floor(Math.random() * 11)
    //         );

    //         vote = {
    //             username: username,
    //             poll_tkn: token,
    //             option: option,
    //         };
    //         try {
    //             await VoteDB.create(vote);
    //         } catch (err) {
    //             console.log(
    //                 "============ error inserting into vote table ============"
    //             );
    //         }

    //         res.status(201).send({ message: "Vote cast for poll " + vote.poll_tkn });
    //     } catch (error) {
    //         res
    //             .status(500)
    //             .send({ message: "Internal server error when casting vote" });
    //     }
    // },

    vote: async(req, res) => {
        try {
            console.log("===============");
            console.log(req.body);

            if (req.body.hasAccess == false) {
                res.status(403).send({ message: "User does not have access to poll" });
            }

            const token = req.body.polltkn;
            const option = req.body.option;
            const user = await jwt.verify(req.body.token, process.env.JWT_SECRET);

            // console.log(user);

            const poll = await PollDB.findByPk(token);

            console.log(poll);

            if (poll) {
                console.log("poll exista");
                if (poll.poll_status == "in progress") {
                    console.log("poll in progress");
                    var votes = await VoteDB.findAll({
                        where: {
                            poll_tkn: token,
                        },
                    });

                    var alreadyVoted = false;
                    var existingUser;

                    // votes.forEach((vote) => {
                    //     if (bcrypt.compare(user.username, vote.username)) {
                    //         alreadyVoted = true;
                    //         existingUser = vote.username;
                    //     }
                    // });

                    // console.log(votes);
                    // console.log(votes.length);

                    for (let i = 0; i < votes.length; i++) {
                        console.log(votes[i].username);
                        if (await bcrypt.compare(user.username, votes[i].username)) {
                            // console.log("a dat true bcrypt.compare");
                            alreadyVoted = true;
                            // console.log("already voted a devenit true");

                            existingUser = votes[i].username;
                            // console.log("existingUser a devenit votes[i].username");
                            break;
                        }
                    }

                    // console.log("======================");
                    // console.log(existingUser);

                    if (alreadyVoted == false) {
                        console.log(
                            "a intrat pe alreadyVoted == false (crede ca nu a votat inca)"
                        );
                        const username = await bcrypt.hash(
                            user.username,
                            Math.floor(Math.random() * 11)
                        );

                        vote = {
                            username: username,
                            poll_tkn: token,
                            option: option,
                        };
                        try {
                            await VoteDB.create(vote);
                        } catch (err) {
                            console.log(
                                "============ error inserting into vote table ============"
                            );
                        }

                        res
                            .status(201)
                            .send({ message: "Vote cast for poll " + vote.poll_tkn });
                    } else {
                        try {
                            console.log("a intrat pe else (crede ca a votat deja)");
                            var vote = await VoteDB.update({ option: option }, {
                                where: {
                                    username: existingUser,
                                    poll_tkn: token,
                                },
                            });

                            res
                                .status(203)
                                .send({ message: "Vote cast for poll " + vote.poll_tkn });
                        } catch (error) {
                            res.status(500).send({
                                message: "Internal server error when casting vote (overwrite)",
                            });
                        }
                    }
                } else {
                    if (poll.poll_status == "closed" || poll.poll_status == "finished") {
                        res.status(423).send({ message: "Voting is not possible" });
                    }
                }
            } else {
                res.status(404).send({ message: "Poll does not exist" });
            }
        } catch (error) {
            console.log("=============error:");
            console.log(error);
            res
                .status(500)
                .send({ message: "Internal server error when casting vote" });
        }
    },

    getPollsUserVotedIn: async(req, res) => {
        try {
            const token = req.body.token;
            const user = await jwt.verify(token, process.env.JWT_SECRET);
            const plainUsername = user.username;

            const votes = await VoteDB.findAll();

            let pollsVotedIn = [];

            for (let i = 0; i < votes.length; i++) {
                if (await bcrypt.compare(plainUsername, votes[i].username)) {
                    const poll = await PollDB.findByPk(votes[i].poll_tkn);
                    pollsVotedIn.push(poll);
                }
            }
            res.status(200).send({ message: "Polls retrieved", polls: pollsVotedIn });
        } catch (error) {
            res
                .status(500)
                .send({ message: "Internal server error when retrieving polls" });
        }
    },

    deleteUser: async(req, res) => {
        console.log("==deleteUser");
        console.log(req.body);
        try {
            UserDB.destroy({
                where: {
                    username: req.body.username,
                },
            });
            res.status(200).send({ message: "User deleted!" });
        } catch (error) {
            res
                .status(500)
                .send({ message: "Internal server error when deleting user" });
        }
    },
};

module.exports = controller;