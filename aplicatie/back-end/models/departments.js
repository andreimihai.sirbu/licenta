module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "department", {
            dep_name: DataTypes.STRING,
            dep_code: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
        }, {
            underscored: true,
            timestamps: false,
        }
    );
};