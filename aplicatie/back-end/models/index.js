const db = require("../config/db");
const Department = db.import("departments");
const User = db.import("users");
const Poll = db.import("polls");
const Options = db.import("options");
const Vote = db.import("votes");

Department.hasMany(User, { as: "children", foreignKey: "dep_code" });
User.belongsTo(Department, {
    as: "parent",
    foreignKey: { name: "dep_code" },
    //as: "DepartmentAlias",
});

Poll.hasMany(Options, { as: "PollOption", foreignKey: "poll_tkn" });
Options.belongsTo(Poll, { as: "OptionPoll", foreignKey: "poll_tkn" });

// User.belongsToMany(Poll, { through: Vote, foreignKey: "user_id" });
// Poll.belongsToMany(User, { through: Vote, foreignKey: "poll_tkn" });
Poll.hasMany(Vote, { as: "PollVote", foreignKey: "poll_tkn" });
Vote.belongsTo(Poll, { as: "VotePoll", foreignKey: "poll_tkn" });

module.exports = {
    //Export pentru reutilizare
    connection: db,
    Department,
    User,
    Poll,
    Options,
    Vote,
};