module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "option", {
            poll_tkn: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            text: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            option_no: DataTypes.INTEGER,
        }, {
            underscored: true,
            timestamps: false,
        }
    );
};