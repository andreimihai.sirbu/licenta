const Sequelize = require("sequelize");
const sequelize = require("../config/db");

module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "poll", {
            tkn: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            question: DataTypes.STRING,
            desc: DataTypes.STRING,
            majority: DataTypes.STRING,
            access_lvl: DataTypes.STRING,
            required_rank: DataTypes.INTEGER,
            open_date: DataTypes.DATE,
            close_date: DataTypes.DATE,
            poll_status: DataTypes.STRING,
            result: DataTypes.STRING,
            quorum: DataTypes.INTEGER,
        }, {
            underscored: true,
            timestamps: false,
        }
    );
};