module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "user", {
            username: {
                type: DataTypes.STRING,
                //primaryKey: true,
                unique: true,
            },
            password: DataTypes.STRING,
            isAdmin: DataTypes.BOOLEAN,
            dep_code: DataTypes.STRING,
            hire_date: DataTypes.STRING,
            position: DataTypes.STRING,
            jobRank: DataTypes.INTEGER,
            hasEverLoggedIn: DataTypes.BOOLEAN,
        }, {
            underscored: true,
            timestamps: false,
        }
    );
};