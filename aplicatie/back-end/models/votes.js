module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "vote", {
            username: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            poll_tkn: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            option: DataTypes.INTEGER,
        }, {
            underscored: true,
            timestamps: false,
        }
    );
};