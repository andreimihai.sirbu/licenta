const express = require("express");
const router = express.Router();
const adminController = require("../controllers").admin;
const middleware = require("../controllers").middleware;

router.post(
    "/createPoll",
    middleware.isAuthenticated,
    middleware.isAdmin,
    adminController.createPoll
);

router.post(
    "/getUsers",
    middleware.isAuthenticated,
    middleware.isAdmin,
    adminController.getAllUsers
);

router.post(
    "/getPolls",
    middleware.isAuthenticated,
    middleware.isAdmin,
    adminController.getAllPolls
);

// router.post(
//     "/uploadCSV",
//     // middleware.isAuthenticated,
//     // middleware.isAdmin,
//     adminController.uploadCSV
// );

router.post(
    "/generateUsers",
    middleware.isAuthenticated,
    middleware.isAdmin,
    adminController.generateUsersFromCSV
);

module.exports = router;