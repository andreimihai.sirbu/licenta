const express = require("express");
const router = express.Router();
const usersRouter = require("./users");
const pollsRouter = require("./polls");
const otherRouter = require("./other");
const adminRouter = require("./admin");
require("dotenv").config();

router.use("/", otherRouter);
router.use("/admin", adminRouter);
router.use("/poll", pollsRouter);
router.use("/user", usersRouter);

module.exports = router;