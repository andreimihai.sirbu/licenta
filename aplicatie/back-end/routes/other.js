const express = require('express')
const router = express.Router()
const connection = require('../models').connection
const UserDB = require("../models").User;
const DepartmentDB = require("../models").Department;
const bcrypt = require("bcrypt")

router.get('/reset', async (req, res) => {
    connection.sync({
            force: true
        })
        .then(async() => {
            await DepartmentDB.create({ dep_code: "IT", dep_name: "IT" });
            var pass = await bcrypt.hash("root123",10)
                await UserDB.create({
                    username: "admin",
                    password: pass,
                    isAdmin: true,
                    dep_code: "IT",
                    hire_date: "17/12/2020",
                    position: "Themis Admin",
                    jobRank: 3,
                    hasEverLoggedIn: false,
                });

            res.status(201).send({
                message: 'S-a resetat baza de date'
            })
        })
        .catch((error) => {
            console.log(error)
            res.status(500).send({
                message: 'Eroare la resetarea BD'
            })
        })
})

module.exports = router