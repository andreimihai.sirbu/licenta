const express = require("express");
const router = express.Router();
const pollController = require("../controllers").polls;
const middleware = require("../controllers").middleware;

// router.post(
//     "/admin/createPoll",
//     middleware.isAuthenticated,
//     middleware.isAdmin,
//     pollController.createPoll
// );

router.put("/updateStatus", pollController.updatePollStatus);

router.post(
    "/checkIfPoll",
    middleware.hasAccessToPoll,
    pollController.checkIfPollExists
);

router.post("/getPoll", pollController.getPoll);

router.post(
    "/getDetails",
    middleware.hasAccessToPoll,
    pollController.getDetails
);

router.post(
    "/editPoll",
    middleware.isAuthenticated,
    middleware.isAdmin,
    pollController.canBeModifiedMiddleware,
    pollController.editPoll
);

router.post("/canBeModified", pollController.canBeModified);

router.post(
    "/deletePoll",
    middleware.isAuthenticated,
    middleware.isAdmin,
    pollController.deletePoll
);

module.exports = router;