const express = require("express");
const router = express.Router();
const userController = require("../controllers").users;
const middleware = require("../controllers").middleware;

router.post("/getDepartments", userController.getDepartments);
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/changePassword", userController.changePassword);
router.get("/profile", checkNotAuth, async(req, res) => {
    res.status(200).send(await req.user);
});
router.post(
    "/vote",
    middleware.isAuthenticated,
    middleware.hasAccessToPoll,
    userController.vote
);

router.post(
    "/getPollsVotedIn",
    middleware.isAuthenticated,
    userController.getPollsUserVotedIn
);

router.post(
    "/update",
    middleware.isAdmin,
    middleware.isAuthenticated,
    userController.updateUser
);

router.post("/getUser", userController.getUser);

router.post(
    "/deleteUser",
    middleware.isAuthenticated,
    middleware.isAdmin,
    userController.deleteUser
);

function checkNotAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/api/notAuth");
}

function checkAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return res.redirect("/api/alreadyAuth");
    }
    return next();
}

async function checkAdmin(req, res, next) {
    const user = await req.user;
    console.log(await req.user);
    if (req.isAuthenticated() && user.is_admin) {
        return next();
    } else res.redirect("/api/notAllowed");
}

module.exports = router;