const express = require("express"); // Importare framework express intr-o variabila constanta
// const bodyParser = require('body-parser')
const router = require("./routes");
const app = express(); // Initializare instanta express pentru aplicatie
const port = 8081;
const cron = require("node-cron");
const cors = require("cors");
const pollController = require("./controllers/polls");
const middleware = require("./controllers/middleware");
require("dotenv").config();

const multer = require("multer");

const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./csv");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "--" + file.originalname);
    },
});

const upload = multer({ storage: fileStorageEngine });

const corsOptions = {
    origin: true,
    allowedHeaders: [
        "Content-Type",
        "Authorization",
        "Access-Control-Allow-Methods",
        "Access-Control-Request-Headers",
    ],
    credentials: true,
    enablePreflight: true,
};

app.use(cors(corsOptions));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080"); // update to match the domain you will make the request from
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

app.use(express.json()); // Orice request trece prin body parser

app.get("/", (req, res) => {
    res.status(200).send("Serveru merge");
});

app.use("/api", router); // Serveste in functie de requestul facut

app.listen(port, () => {
    console.log("Serverul ruleaza pe portul: " + port);
});

app.post("/api/admin/uploadCSV", upload.single("file"), async(req, res) => {
    try {
        console.log(req.file.filename);
        res.status(200).send({
            message: "CSV uploaded successfully",
            filename: req.file.filename,
        });
    } catch (error) {
        console.log(error);
        res
            .status(500)
            .send({ message: "Internal server error when receiving file upload" });
    }
});

// cron.schedule("* * * * *", async(req, res) => {
//     console.log("running a task every minute");
//     const current_date = new Date();
//     console.log(current_date);

//     const req_body = {
//         current_date: current_date,
//     };
//     // const res = await fetch("")
//     const result = await pollController.updatePollStatus(current_date, res);
//     if (result == null)
//         res.status(500).send({ message: "internal server error updating polls" });
// });