import store from "../store/storeUser";

const routes = [{
        path: "/",
        component: () =>
            import ("layouts/MainLayout.vue"),
        children: [{
                path: "home",
                component: () =>
                    import ("pages/Home.vue")
            },
            {
                path: "profile",
                component: () =>
                    import ("pages/Profile.vue")
            },
            {
                path: "results",
                component: () =>
                    import ("pages/Results copy.vue")
            },
            {
                path: "admin",
                component: () =>
                    import ("pages/Admin.vue"),
                beforeEnter: (to, from, next) => {
                    if (store.state.user.admin == false) {
                        console.log("crede ca nu sunt admin");
                        console.log(store.state);
                        next("/home");
                    } else {
                        console.log("crede ca sunt admin");
                        console.log(store.state.user);
                        next();
                    }
                }
            },
            {
                path: "admin/createpoll",
                component: () =>
                    import ("pages/CreatePoll.vue"),
                beforeEnter: (to, from, next) => {
                    if (store.state.user.admin == false) {
                        next("/home");
                    } else {
                        next();
                    }
                }
            },
            {
                path: "admin/adduser",
                component: () =>
                    import ("pages/AddUser.vue"),
                beforeEnter: (to, from, next) => {
                    if (store.state.user.admin == false) {
                        next("/home");
                    } else {
                        next();
                    }
                }
            },
            {
                path: "admin/edituser/:username",
                component: () =>
                    import ("pages/EditUser.vue"),
                beforeEnter: (to, from, next) => {
                    if (store.state.user.admin == false) {
                        next("/home");
                    } else {
                        next();
                    }
                }
            },
            {
                path: "admin/editpoll/:token",
                component: () =>
                    import ("pages/EditPoll.vue"),
                beforeEnter: (to, from, next) => {
                    if (store.state.user.admin == false) {
                        next("/home");
                    } else {
                        next();
                    }
                }
            },
            {
                path: "vote/:token",
                component: () =>
                    import ("pages/Vote.vue")
            },
            {
                path: "onsubmit",
                component: () =>
                    import ("pages/OnSubmit.vue")
            },
            {
                path: "details/:token",
                component: () =>
                    import ("pages/ResultDetails.vue")
            }
        ],
        beforeEnter: (to, from, next) => {
            if (!window.sessionStorage.getItem("token")) {
                next("/login");
            } else {
                next();
            }
        }
    },
    {
        path: "/login",
        component: () =>
            import ("layouts/AuthLayout.vue"),
        children: [{
            path: "",
            component: () =>
                import ("pages/Login.vue")
        }]
    },
    {
        path: "/setpassword",
        component: () =>
            import ("layouts/AuthLayout.vue"),
        children: [{
            path: "",
            component: () =>
                import ("pages/SetPassword.vue")
        }]
    },
    {
        path: "/resetpassword",
        component: () =>
            import ("layouts/AuthLayout.vue"),
        children: [{
            path: "",
            component: () =>
                import ("pages/ResetPassword.vue")
        }]
    },

    // {
    //   path: '/',
    //   component: () => import('layouts/MainLayout.vue'),
    //   children: [
    //     { path: '', component: () => import('pages/Results.vue') }
    //   ]
    // },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: "*",
        component: () =>
            import ("pages/Error404.vue")
    }
];

export default routes;