import Axios from "axios";

const state = {
    authentificated: false
};
const getters = {
    getProfile: state => state
};
const actions = {};
const mutations = {
    setAuthentification(state, status) {
        state.authentificated = status;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};