import { LocalStorage, SessionStorage } from "quasar";

export function setState(state) {
    state.token = SessionStorage.getItem("token");
}