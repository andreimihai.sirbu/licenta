import { LocalStorage, SessionStorage } from "quasar";

export default {
    token: SessionStorage.getItem("token")
};