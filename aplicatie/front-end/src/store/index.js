// import { createStore } from "vuex";

// import auth from "./auth/autheStore";
// import profile from "./profile/profileStore";
import Vue from "vue";
import Vuex from "vuex";
import user from "./storeUser";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

// export default new Vuex.Store({
//     modules: {
//         user
//     },
//     plugins: [
//         createPersistedState({
//             paths: ["user.login"],
//             storage: window.sessionStorage
//         })
//     ],
//     strict: process.env.DEBUGGING
// });

export default new Vuex.Store({
    plugins: [
        createPersistedState({
            storage: window.sessionStorage
        })
    ],
    modules: {
        user
    }
});

// export default function( /* { ssrContext } */ ) {
//     const Store = new Vuex.Store({
//         modules: {
//             user
//         },
//         plugins: [createPersistedState()],
//         // enable strict mode (adds overhead!)
//         // for dev mode only
//         strict: process.env.DEBUGGING
//     });

//     return Store;
// }