import { api } from "boot/axios";

// const state = user ?
//     { status: { loggedIn: true }, user } :
//     { status: {}, user: null };

// const mutations = {
//     login(state, user) {
//         state.status = { loggedIn: true };
//         state.user = user;
//     },
//     logout(state) {
//         state.status = {};
//         state.user;
//     }
// };

const state = {
    user: {
        username: "",
        department: "",
        admin: "",
        hireDate: "",
        position: "",
        jobRank: ""
    }
};

const mutations = {
    login(state, user) {
        state.user.username = user.username;
        state.user.department = user.dep_code;
        state.user.admin = user.isAdmin;
        state.user.hireDate = user.hire_date;
        state.user.position = user.position;
        state.user.jobRank = user.jobRank;

        // window.sessionStorage.setItem("vuex", state.user);
    },

    logout(state) {
        state.user = null;
    }
};

const actions = {};

const getters = {
    username: state => {
        return state.user.username;
    },
    department: state => {
        return state.user.department;
    },
    admin: state => {
        return state.user.admin;
    },
    hireDate: state => {
        return state.user.hireDate;
    },
    position: state => {
        return state.user.position;
    },
    jobRank: state => {
        return state.user.jobRank;
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};